<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembelianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembelian', function (Blueprint $table) {
            $table->char('no_pembelian', 15);
            $table->date('tanggal');
            $table->integer('id_supplier', 11);
            $table->integer('id_barang', 11);
            $table->integer('jumlah_barang', 11);
            $table->integer('harga_barang', 11)->default;
            $table->date('create_at')->default;;
            $table->char('create_by')->default;;
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembelian');
    }
}
